﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speedupVFX : PooledObject
{

    [SerializeField]
    private ParticleSystem particles;

    private Transform initialParent;

    public void Play(Racer racer)
    {
        initialParent = transform.parent;

        transform.parent = racer.transform;

        transform.rotation = racer.transform.rotation;

        transform.localPosition = Vector3.up;

        gameObject.SetActive(true);

        particles.Play();

        StartCoroutine(Duration());
    }

    private IEnumerator Duration()
    {
        yield return new WaitForSeconds(particles.main.duration * .9f);

        particles.Pause();

        transform.parent = initialParent;

        yield return new WaitForSeconds(particles.main.duration);


        gameObject.SetActive(false);
    }
}
