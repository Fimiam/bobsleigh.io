﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ParticlesVFXObject : PooledObject
{
    [SerializeField]
    private ParticleSystem effect;

    public void Play(Vector3 position, Quaternion rotation)
    {
        transform.position = position;

        transform.rotation = rotation;

        gameObject.SetActive(true);

        var sequnce = DOTween.Sequence();

        sequnce.onComplete += () => gameObject.SetActive(false);

        sequnce.AppendInterval(effect.main.duration);
    }
}
