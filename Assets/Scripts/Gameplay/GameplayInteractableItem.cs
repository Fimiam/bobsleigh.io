﻿using UnityEngine;

public abstract class GameplayInteractableItem : Interactable
{
    protected InteractablesController interactableController;

    public void Setup(InteractablesController interactablesController)
    {
        this.interactableController = interactablesController;
    }

    private void OnTriggerEnter(Collider other)
    {
        Racer racer = other.GetComponent<Racer>();

        if (racer != null) RacerInteraction(racer);
    }

    public abstract void RacerInteraction(Racer racer);

    public abstract void PlayerInteracted(ProgressController progressController);
}
