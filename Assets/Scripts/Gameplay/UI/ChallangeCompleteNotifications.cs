﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChallangeCompleteNotifications : MonoBehaviour
{
    [SerializeField] private List<ChallangeCompleteGameplayNotification> notificationsPool;
    
    private void Start()
    {
        Game.Instance.Challanges.OnChallangeApplied += ChallangeApplied;
    }

    private void ChallangeApplied(ChallangeType type, int value)
    {
        var completeChallange =
            Game.Instance.Configurations.Challanges.challanges
                .Find(c => c.Type == type && c.CompleteValue == value);

        if (completeChallange == null) return;
        
        
        var element = notificationsPool.Find(e => !e.gameObject.activeSelf);
        
        notificationsPool.FindAll(e => e.gameObject.activeSelf)
            .ForEach(e => e.Hide());
        
        if (element != null)
        {
            element.Show(completeChallange);
        }
    }

    private void OnDestroy()
    {
        if(Game.Instance != null)
            Game.Instance.Challanges.OnChallangeApplied -= ChallangeApplied;
    }
}
