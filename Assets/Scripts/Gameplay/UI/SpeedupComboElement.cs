﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using  DG.Tweening;

public class SpeedupComboElement : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI descr_text, combo_text;
    [SerializeField] private new RectTransform transform;
    [SerializeField] private RectTransform textContainer;
    [SerializeField] private CanvasGroup textGroup;

    private Sequence sequence;
    
    private float fadingOffset = 250;
    
    public void Show(Vector3 anchPosition, int combo, float duration)
    {
        transform.anchoredPosition = anchPosition;

        textGroup.alpha = 0;

        descr_text.text = Game.Instance.Configurations.SpeedupsCombo.GetComboText(combo);
        combo_text.text = $"x {combo}";

        textContainer.anchoredPosition = Vector2.right * -fadingOffset;
        
        gameObject.SetActive(true);

        sequence = DOTween.Sequence();

        sequence.onComplete += Hide;

        sequence.Append(textContainer.DOAnchorPos(Vector2.zero, .3f));
        sequence.Join(textGroup.DOFade(1, .3f));
        sequence.AppendInterval(duration);
    }

    public void Hide()
    {
        sequence.Kill();
        
        textContainer.DOAnchorPos(Vector2.right * fadingOffset, .3f)
            .onComplete += () => gameObject.SetActive(false);
        textGroup.DOFade(0, .3f);
    }
}
