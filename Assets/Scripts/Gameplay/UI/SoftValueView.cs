﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class SoftValueView : UIElement
{
    private new RectTransform transform;

    private int currentValue = 0;

    private new Camera camera;

    private Canvas canvas;
    
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private SoftCollectionPool effectsPool;
    
    private int waitingForSoft;

    private float waitSoftDuration = 2.0f, waitLast;

    private void Start()
    {
        transform = GetComponent<RectTransform>();

        text.text = currentValue.ToString("D5");
    }

    public void AddingSoft(int value, Vector3 collectPos, bool addingEffect = false)
    {
        if(transform.anchoredPosition != shownValues)
            Show();

        if (addingEffect)
        {
            var effect = effectsPool.GetObject();
        
            effect.ShowEffect(value, collectPos, this);
            
            waitingForSoft++;
        }
        else
        {
            if (transform.anchoredPosition != shownValues)
            {
                var sequence = DOTween.Sequence();

                sequence.onComplete += () => AddValueEasy(value);
                
                sequence.AppendInterval(.2f);
            }
            else
            {
                AddValueEasy(value);
            }
        }

        waitLast = waitSoftDuration;
    }

    private void Update()
    {
        if (shown && waitingForSoft < 1)
        {
            waitLast -= Time.deltaTime;

            if (waitLast < 0)
            {
                Hide();
            }
        }
    }

    public void AddValueEasy(int value)
    {
        currentValue += value;

        text.text = currentValue.ToString("D5");
        
        text.transform.DOPunchScale(Vector3.one * .3f, .1f, 1, 1);
    }
    
    public void AddValue(int value)
    {
        waitingForSoft--;

        text.transform.DOComplete();
        transform.DOComplete();
        transform.localScale = Vector3.one;
        
        if (waitingForSoft < 1)
        {
            waitingForSoft = 0;

            transform.DOPunchScale(Vector3.one * .5f, .5f, 1, 1)
                .onComplete += () =>
                {
                    Hide();
                };
        }
        else
        {
            text.transform.DOPunchScale(Vector3.one * .3f, .1f, 1, 3);
        }

        currentValue += value;

        text.text = currentValue.ToString("D5");
    }

    public Vector2 GetShownScreenPos()
    {
        Vector2 pos = Vector2.zero;

        var canvasRect = canvas.GetComponent<RectTransform>();

        pos.y = ((canvasRect.sizeDelta.y - shownValues.y - transform.sizeDelta.y) / canvasRect.sizeDelta.y) * Screen.height;
        
        pos.x = ((canvasRect.sizeDelta.x - shownValues.x - transform.sizeDelta.x * .5f) / canvasRect.sizeDelta.x) * Screen.width;
        
        
        return pos;
    }

    public void Setup(Canvas canvas, Camera cam)
    {
        camera = cam;
        this.canvas = canvas;

        effectsPool.GetPool().ForEach(e => e.Setup(cam, canvas));
    }
}
