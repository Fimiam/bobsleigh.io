﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIElement : MonoBehaviour
{
    [SerializeField]
    protected Vector2 hidenValues, shownValues;

    protected bool shown;

    [SerializeField]
    protected float hidingAnimDuration = .6f, showingAnimDuration = .6f;


    protected RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();

        shown = rectTransform.anchoredPosition == shownValues;
    }

    public virtual void Show()
    {
        shown = true;
        DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, shownValues, showingAnimDuration);
    }

    public virtual void Hide()
    {
        shown = false;
        DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, hidenValues, hidingAnimDuration);
    }
}
