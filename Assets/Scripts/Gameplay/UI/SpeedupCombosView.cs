﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedupCombosView : MonoBehaviour
{
    [SerializeField] private List<SpeedupComboElement> elements;
    [SerializeField] private Vector2 defaultPos;
    
    public void Show(int combo, float duration)
    {
        var element = elements.Find(e => !e.gameObject.activeSelf);
        
        elements.FindAll(e => e.gameObject.activeSelf)
            .ForEach(e => e.Hide());
        
        if (element != null)
        {
            element.Show(defaultPos, combo, duration);
        }
    }
}
