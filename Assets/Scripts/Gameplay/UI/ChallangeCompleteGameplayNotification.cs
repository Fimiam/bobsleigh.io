﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class ChallangeCompleteGameplayNotification : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI description_text;
    [SerializeField] private CanvasGroup group;
    [SerializeField] private float hidenXPos = -500, duration = 2, fadeDuration = .4f;
    [SerializeField] private new RectTransform containerTransform;

    private Sequence sequence;
    
    public void Show(ChallangeConfig completeChallange)
    {
        containerTransform.anchoredPosition = Vector2.right * hidenXPos;

        group.alpha = 0;

        description_text.text = completeChallange.Description;

        gameObject.SetActive(true);
        
        sequence = DOTween.Sequence();

        sequence.onComplete += Hide;

        sequence.Append(containerTransform.DOAnchorPos(Vector2.zero, fadeDuration));
        sequence.Join(group.DOFade(1, fadeDuration));
        sequence.AppendInterval(duration);
    }

    public void Hide()
    {
        sequence.Kill();

        sequence = DOTween.Sequence();

        sequence.onComplete += () => gameObject.SetActive(false);
        
        sequence.Append(containerTransform.DOAnchorPos(Vector2.right * hidenXPos, fadeDuration));
        sequence.Join(group.DOFade(0, fadeDuration));
    }
}
