﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class Gameplay : MonoBehaviour
{
    private LevelConfig level;

    [SerializeField]
    private List<GameplayController> controllers;

    public LevelConfig Level => level;

    public T GetController<T>() where T : GameplayController
    {
        T temp;

        temp = controllers.Find(c => c.GetType() == typeof(T)) as T;

        if (temp == null)
            Debug.LogError("there is no controller of this type - " + typeof(T));

        return temp;
    }

    public void LevelLoaded()
    {
        controllers.ForEach(c => c.LevelLoaded());
    }

    public void PlayerSpawned()
    {
        controllers.ForEach(c => c.PlayerSpawned());
    }

    public void StartRaceReques()
    {
        controllers.ForEach(c => c.RaceBegins());
    }

    private void Awake()
    {
        //level = Game.Instance.GetCurrentLevel();

        controllers.ForEach(c => c.Setup(this));
    }

    public void LocationLoaded()
    {
        controllers.ForEach(c => c.LocationLoaded());

        controllers.ForEach(c => c.Init());
    }

    public void TrackLoaded()
    {
        controllers.ForEach(c => c.TrackLoaded());
    }

    public void LevelComplete()
    {
        controllers.ForEach(c => c.LevelComplete());

        Invoke("Restart", 2.5f);
    }

    public void PlayerFreefly()
    {
        controllers.ForEach(c => c.PlayerFreefly());
    }

    public void RestartRequest()
    {
        Restart();
    }


    public void HomeRequest()
    {
        Game.Instance.LevelComplete();
    }

    private void Restart()
    {
        DG.Tweening.DOTween.KillAll();

        ScenesManager.Instance.LoadScene(gameObject.scene.buildIndex);
    }
}
