﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sleigh : MonoBehaviour
{
    [SerializeField]
    private new Collider collider;

    [SerializeField]
    private Transform driverContainer;

    private Racer racer;

    public Transform DriverContainer => driverContainer;

    private bool landingChecking;

    private float landingCheckingDepth = 2;

    private void Awake()
    {
        collider.enabled = false;
    }

    //private void Update()
    //{
    //    if(landingChecking)
    //    {
    //        var ray = new Ray(transform.position, Vector3.down);

    //        Debug.DrawRay(ray.origin, ray.direction * landingCheckingDepth, Color.gray, 2);

    //        if (Physics.Raycast(ray, out RaycastHit hit, landingCheckingDepth))
    //        {
    //            var surface = hit.collider.GetComponent<RoadSurface>();

    //            if(surface != null)
    //            {
    //                EndLandingCheck();

    //                surface.Road.RacerLanded(racer);
    //            }
    //        }
    //    }
    //}

    public void SetRacer(Racer racer)
    {
        this.racer = racer;
    }

    public void LeftRoad()
    {
        Debug.Log("sleigh leftRoad");

        Invoke("LandingCheck", .25f);
    }

    private void LandingCheck()
    {
        collider.enabled = true;
    }

    private void EndLandingCheck()
    {
        collider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        var surface = other.GetComponent<RoadSurface>();

        if(surface != null)
        {
            EndLandingCheck();
            surface.Road.RacerLanded(racer);
        }
    }
}
