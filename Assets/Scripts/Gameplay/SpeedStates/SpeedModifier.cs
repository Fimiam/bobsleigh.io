﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpeedModifier
{
    protected Racer racer;

    protected float speedValue;

    protected float speed;

    public float Speed => speed;

    private SpeedType type;

    public SpeedType SpeedType => type;

    public SpeedModifier(Racer racer, float endValue, SpeedType speedType)
    {
        this.speedValue = endValue;
        this.racer = racer;
        type = speedType;
    }

    public virtual void EndState()
    {
        racer.RemoveSpeedModifier(this);
    }
}

public enum SpeedType
{
    FORWARD, SIDE, GRAVITY
}
