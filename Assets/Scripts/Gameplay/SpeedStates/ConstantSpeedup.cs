﻿using UnityEngine;
using UnityEditor;
using DG.Tweening;

public class ConstantSpeedup : SpeedModifier
{
    public ConstantSpeedup(Racer racer, float endValue, SpeedType speedType, float speedupDuration) :
        base(racer, endValue, speedType)
    {
        if (speedupDuration == 0)
        {
            speed = endValue;
        }
        else
        {
            DOTween.To(() => speed, x => speed = x, endValue, speedupDuration);
        }
    }
}