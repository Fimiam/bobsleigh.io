﻿using UnityEngine;
using UnityEditor;

public class ConstantSpeed : SpeedModifier
{
    public ConstantSpeed(Racer racer, float endValue, SpeedType speedType) :
        base(racer, endValue, speedType)
    {
        speed = endValue;
    }
}