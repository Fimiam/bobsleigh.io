﻿using UnityEngine;
using UnityEditor;
using DG.Tweening;

public class TwoTransitionSpeed : SpeedModifier
{

    public TwoTransitionSpeed(Racer racer, float endValue, SpeedType speedType, float firstValue, float firstAccTime = 1, float endAccTime = 1) :
        base(racer, endValue, speedType)
    {
        var sequence = DOTween.Sequence();

        sequence.Append(DOTween.To(() => speed, x => speed = x, firstValue, firstAccTime));
        sequence.Append(DOTween.To(() => speed, x => speed = x, endValue, endAccTime));
    }
}