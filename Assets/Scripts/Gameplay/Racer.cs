﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racer : MonoBehaviour
{
    protected RacerState state;

    public RacerState State => state;

    protected RacerBehaviour behaviour;

    protected MovementBehaviour movement;

    public MovementBehaviour Movement => movement;

    protected InputModule inputModule;

    public InputModule InputModule => inputModule;

    protected RacersController racersController;

    protected bool isPlayer;

    public bool IsPlayer => isPlayer;

    private Sleigh sleigh;
    private Driver driver;


    public Sleigh Sleigh => sleigh;
    public Driver Driver => driver;

    public virtual void Setup(RacersController racersController)
    {
        this.racersController = racersController;

        movement = new RoadMovement(this);
    }

    public void AddSpeedModifier(SpeedModifier modifier)
    {
        behaviour.AddSpeedModifier(modifier);
    }

    public void RemoveSpeedModifier(SpeedModifier modifier)
    {
        behaviour.RemoveSpeedModifier(modifier);
    }

    public void SetRoad(Road road, float distance = 0, float sideSift = 0)
    {
        state.Road = road;
        state.Distance = distance;
        state.SideShift = sideSift;
    }

    public virtual void LandingOnRoad(Road road, float distance, float sideShift)
    {
        SetRoad(road, distance, sideShift);
    }

    public void Update()
    {
        inputModule.ProcessInput();

        behaviour.UpdateBehaviour();

        movement.Update();
    }

    public virtual void OutroadJump()
    {
        behaviour.OutroadJump();
    }

    public virtual void OutroadFallout()
    {
        Debug.Log("outroad fallout");

        behaviour.OutroadFallout();
    }

    public virtual void Landed()
    {
        
    }

    public void RaceBegins()
    {
        behaviour.AddSpeedModifier(new ConstantSpeedup(this, 25, SpeedType.FORWARD ,3));
        behaviour.AddSpeedModifier(new ConstantSpeed(this, 25, SpeedType.SIDE));
    }

    public void SetDriver(Driver driver)
    {
        var oldDriver = this.driver;

        if (sleigh == null)
        {
            Debug.LogError("there is no sleigh to set a driver on");
            return;
        }

        this.driver = driver;

        driver.transform.parent = sleigh.DriverContainer;

        driver.transform.localPosition = Vector3.zero;
        driver.transform.rotation = sleigh.transform.rotation;

        if(oldDriver != null) Destroy(oldDriver.gameObject);
    }

    public void SetSleight(Sleigh sleigh)
    {
        var oldSleigh = this.sleigh;

        this.sleigh = sleigh;

        this.sleigh.SetRacer(this);

        sleigh.transform.parent = transform;
        sleigh.transform.localPosition = Vector3.zero;

        if (driver != null)
        {
            driver.transform.parent = sleigh.transform;
            driver.transform.position = sleigh.DriverContainer.position;
            driver.transform.rotation = sleigh.DriverContainer.rotation;
        }
        else
        {
            Debug.LogWarning("there is no driver to set on the sleigh");
        }

        if (oldSleigh != null) Destroy(oldSleigh.gameObject);
    }
}
