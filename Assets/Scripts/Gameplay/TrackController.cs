﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackController : GameplayController
{
    private Track track;

    private LevelController levelController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        levelController = gameplay.GetController<LevelController>();
    }

    public override void LevelLoaded()
    {
        base.LevelLoaded();

        track = levelController.Level.Track;

        track.Setup();

        gameplay.TrackLoaded();
    }
}
