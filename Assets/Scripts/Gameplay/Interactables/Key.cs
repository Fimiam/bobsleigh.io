﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : CollectableObject
{
    [SerializeField]
    private Transform model;

    [SerializeField]
    private float rotSpeed = 45.0f;


    private void FixedUpdate()
    {
        model.Rotate(Vector3.up * rotSpeed * Time.deltaTime);
    }

    public override void RacerInteraction(Racer racer)
    {
        if (collected) return;

        collected = interactableController.IsItemInteracted(this, racer);

        if(collected)
        {
            gameObject.SetActive(false);
        }
    }

    public override void PlayerInteracted(ProgressController progressController)
    {
        progressController.AddKey();
    }
}
