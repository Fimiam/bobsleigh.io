﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soft : CollectableObject
{

    private int value = 1;

    public int Value => value;
    
    private void OnEnable()
    {
        collected = false;
    }

    public override void RacerInteraction(Racer racer)
    {
        if (collected) return;

        collected = interactableController.IsItemInteracted(this, racer);

        if(collected)
        {
            gameObject.SetActive(false);
        }
    }

    public override void PlayerInteracted(ProgressController progressController)
    {
        progressController.AddSoft(this);
    }
}
