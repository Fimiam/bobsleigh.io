﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Speedup : GameplayInteractableItem
{
    [SerializeField]
    private Transform text;

    [SerializeField]
    private float punchPeriod = 3.0f;

    private SpeedModifier accState, duratedState;

    private void Start()
    {
        text.DOPunchScale(Vector3.one * .3f, punchPeriod, 2).SetLoops(-1);
    }

    public override void RacerInteraction(Racer racer)
    {
        interactableController.IsItemInteracted(this, racer);

        accState = new TwoTransitionSpeed(racer, 0, SpeedType.FORWARD, 12, .1f, 3f);
        duratedState = new TwoTransitionSpeed(racer, 0, SpeedType.FORWARD, 6, .1f, 30f);

        racer.AddSpeedModifier(accState);
        racer.AddSpeedModifier(duratedState);

        StartCoroutine(EndState(accState, 4f));
        StartCoroutine(EndState(duratedState, 30f));
    }

    private IEnumerator EndState(SpeedModifier modifier ,float interval)
    {
        for(float i = 0; i < interval; i += Time.deltaTime)
        {
            yield return null;
        }

        modifier.EndState();
    }

    public override void PlayerInteracted(ProgressController progressController)
    {
        progressController.PlayerDidSpeedup();
    }
}
