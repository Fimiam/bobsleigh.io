﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : GameplayController
{
    private PlayerController playerController;
    private LevelController levelController;

    private Racer target;

    private new Camera camera;

    [SerializeField]
    private float cameraSpeed = 6.0f, rotSpeed = 5.0f;

    [SerializeField]
    private Transform cameraContainer;

    private Vector3 cameraPosition, targetPosition;

    private Quaternion cameraRotation, targetRotation;

    [SerializeField]
    private Vector3 cameraOffset;

    private bool chasingTarget;

    public Camera Camera => camera;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        playerController = gameplay.GetController<PlayerController>();
        levelController = gameplay.GetController<LevelController>();
    }

    public override void LevelLoaded()
    {
        base.LevelLoaded();

        camera = levelController.Level.Camera;

        cameraContainer.position = camera.transform.position;
        cameraContainer.rotation = camera.transform.rotation;

        camera.transform.parent = cameraContainer;
        camera.transform.localPosition = Vector3.zero;

        cameraPosition = cameraContainer.position;

        cameraRotation = cameraContainer.rotation;
    }

    public override void PlayerSpawned()
    {
        base.PlayerSpawned();

        target = playerController.Player;
    }

    private void LateUpdate()
    {
        if(isRacing && chasingTarget)
        {
            float finalCamSpeed = cameraSpeed;

            if(playerFreefly)
            {
                targetPosition = target.transform.position + target.transform.rotation * cameraOffset;

                targetRotation = Quaternion.LookRotation(target.transform.position - cameraContainer.position);//  cameraContainer.LookAt(target.transform.position);

                finalCamSpeed *= .8f;
            }
            else
            {
                targetPosition = target.transform.position + target.Movement.RoadPoint.rotation * cameraOffset;

                targetRotation = Quaternion.LookRotation(Vector3.Lerp(target.transform.position, target.Movement.RoadPoint.position, .5f) - cameraContainer.position);
            }

            cameraPosition = Vector3.Lerp(cameraPosition, targetPosition, finalCamSpeed * Time.deltaTime);

            cameraRotation = Quaternion.Lerp(cameraRotation, targetRotation, rotSpeed * Time.deltaTime);

            cameraContainer.position = cameraPosition;

            cameraContainer.rotation = cameraRotation;
        }
    }

    public override void RaceBegins()
    {
        base.RaceBegins();

        chasingTarget = true;
    }


}
