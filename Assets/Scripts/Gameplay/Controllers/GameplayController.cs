﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameplayController : MonoBehaviour
{
    protected Gameplay gameplay;

    protected bool isRacing, playerFreefly;

    public virtual void Setup(Gameplay gameplay)
    {
        this.gameplay = gameplay;
    }

    public virtual void LocationLoaded() { }
    public virtual void Init() { }
    public virtual void LevelComplete() { isRacing = false; }
    public virtual void LevelLoaded() { }
    public virtual void TrackLoaded() { }
    public virtual void PlayerSpawned() { }
    public virtual void ShootReady() { }
    public virtual void PlayerFreefly() { playerFreefly = true; }
    public virtual void PlayerLanded() { playerFreefly = false; }
    public virtual void RaceBegins() { isRacing = true; }
}
