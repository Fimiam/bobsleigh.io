﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : GameplayController
{
    private LevelController levelController;

    private RacersController racersController;

    private InputController inputController;

    private PlayerRacer player;

    public PlayerRacer Player => player;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        racersController = gameplay.GetController<RacersController>();
        levelController = gameplay.GetController<LevelController>();
        inputController = gameplay.GetController<InputController>();
    }

    public override void LevelLoaded()
    {
        base.LevelLoaded();
    }

    public override void TrackLoaded()
    {
        base.TrackLoaded();

        Road road = levelController.Level.StartRoad;

        float distance = road.GetDistanceAtTime(levelController.Level.StartProgress);

        var racerModel = racersController.GetRacer();

        player = racerModel.gameObject.AddComponent<PlayerRacer>();

        player.Setup(racersController, inputController, this);

        player.SetRoad(road, distance);

        Sleigh sleigh = Instantiate(Game.Instance.GetSleigh());
        Driver driver = Instantiate(Game.Instance.GetDriver());

        player.SetSleight(sleigh);

        player.SetDriver(driver);

        gameplay.PlayerSpawned();
    }

    public void PlayerOutroad()
    {
        Game.Instance.Vibrations.PlayerOutroadVibration();
        gameplay.PlayerFreefly();
    }

    public override void RaceBegins()
    {
        base.RaceBegins();

        player.RaceBegins();
    }
}
