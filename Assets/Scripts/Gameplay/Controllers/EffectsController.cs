﻿using UnityEngine;

public class EffectsController : GameplayController
{
    [SerializeField]
    private ParticlesVFXPool softVFXPool, keyVFXPool, hardVFXPool;

    [SerializeField]
    private SpeedupVFXPool speedupsPool;

    public void PlayInteractableVFX(GameplayInteractableItem item, Racer racer)
    {
        switch (item.InteractableType)
        {
            case Interactables.SOFT:

                softVFXPool.GetObject().Play(item.transform.position, Quaternion.identity);

                break;
            case Interactables.HARD:

                hardVFXPool.GetObject().Play(item.transform.position, Quaternion.identity);

                break;
            case Interactables.KEY:

                keyVFXPool.GetObject().Play(item.transform.position, Quaternion.identity);

                break;
            case Interactables.SPEEDUP:

                speedupsPool.GetObject().Play(racer);

                break;
        }
    }
}
