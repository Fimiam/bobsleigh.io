﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressController : GameplayController
{

    private int currentSpeedupsCombo;

    private float speedupsComboTime, speedupsComboLast;
    
    private SpeedupCombosView speedupCombosView;

    public SoftHandler SoftHandler { get; private set; }
    

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        SoftHandler = new SoftHandler(gameplay.GetController<UIController>().SoftView);
        
        speedupCombosView = gameplay.GetController<UIController>().SpeedupCombosView;

        speedupsComboTime = Game.Instance.Configurations.SpeedupsCombo.ComboLastDuration;
        
    }

    public void AddSoft(Soft soft)
    {
        SoftHandler.AddSoft(soft);
    }

    public void AddKey()
    {
        Debug.Log("player has a key");
    }

    public void PlayerDidSpeedup()
    {
        speedupsComboLast = speedupsComboTime;

        currentSpeedupsCombo++;

        if (currentSpeedupsCombo > 1)
        {
            speedupCombosView.Show(currentSpeedupsCombo, speedupsComboTime);
        }
    }

    private void Update()
    {
        speedupsComboLast -= Time.deltaTime;

        if (speedupsComboLast < 0)
        {
            currentSpeedupsCombo = 0;
        }
    }
}
