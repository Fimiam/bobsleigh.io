﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : GameplayController
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private SoftValueView softView;
    [SerializeField] private SpeedupCombosView speedupCombosView;
    public SoftValueView SoftView => softView;
    
    public SpeedupCombosView SpeedupCombosView => speedupCombosView;

    private LevelController levelController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        levelController = gameplay.GetController<LevelController>();
    }

    public override void LevelLoaded()
    {
        base.LevelLoaded();

        Camera cam = levelController.Level.Camera;

        //canvas.worldCamera = cam;

        softView.Setup(canvas, cam);
    }
}
