﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacersController : GameplayController
{
    [SerializeField]
    private RacersPool pool;

    private int racersCount;

    private List<Racer> racers = new List<Racer>();

    private List<RacerState> racerStates = new List<RacerState>();

    private RacersMover mover;

    public List<RacerState> RacerStates => racerStates;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        //mover = new RacersMover();
    }

    public RacerModel GetRacer()
    {
        RacerModel racer = pool.GetObject();

        racer.gameObject.SetActive(true);

        racersCount++;

        return racer;
    }

    private void Update()
    {

    }

    public override void RaceBegins()
    {
        base.RaceBegins();
    }
}


