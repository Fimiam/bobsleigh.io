﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : GameplayController
{
    private CameraController cameraController;

    public bool Hold
    {
        get
        {
            return Input.GetMouseButton(0);
        }
    }

    public bool HaveTap
    {
        get
        {
            return Input.GetMouseButtonDown(0);
        }
    }

    private Vector2 InputPosition
    {
        get
        {
            return cameraController.Camera.ScreenToViewportPoint(Input.mousePosition);
        }
    }

    public bool IsPointerOverUI
    {
        get
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return EventSystem.current.IsPointerOverGameObject();
#else
            return IsPointerOverUIObject();
#endif
        }
    }

    private Vector3 tapPoint, currPoint, prevPoint;

    public Vector2 InputDelta { get; private set; }

    private bool waitingForChoice;

    private float totalInputDistance;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);
        cameraController = gameplay.GetController<CameraController>();
    }

    private void Update()
    {

        if(HaveTap)
        {
            prevPoint = Input.mousePosition;
        }


        if(Hold)
        {
            currPoint = Input.mousePosition;

            InputDelta = (currPoint - prevPoint) / Screen.width;

            prevPoint = currPoint;
        }
        else
        {
            InputDelta = Vector2.zero;
        }

    }

    private bool IsPointerOverUIObject()
    {
        if (Input.touchCount < 1) return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }


}
