﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablesController : GameplayController
{
    private ProgressController progressController;

    private LevelController levelController;

    private EffectsController effectsController;

    [SerializeField]
    private SoftPool softPool;
    [SerializeField]
    private SpeedupsPool speedupsPool;
    [SerializeField]
    private KeysPool keysPool;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        progressController = gameplay.GetController<ProgressController>();

        levelController = gameplay.GetController<LevelController>();

        effectsController = gameplay.GetController<EffectsController>();
    }

    public override void LevelLoaded()
    {
        base.LevelLoaded();

        levelController.Level.SpawnInteractables(this);
    }

    public bool IsItemInteracted(GameplayInteractableItem item, Racer racer)
    {
        if(racer.IsPlayer)
        {
            item.PlayerInteracted(progressController);

            effectsController.PlayInteractableVFX(item, racer);

            return true;
        }

        return false;
    }

    public void InteractableSpawn(Interactables type, SpawnItemData spawnData)
    {
        GameplayInteractableItem item = null;
        
        Game.Instance.Vibrations.ItemInteractionVibro();

        switch (type)
        {
            case Interactables.SOFT:

                item = softPool.GetObject();

                break;
            case Interactables.HARD:
                break;
            case Interactables.SPEEDUP:

                item = speedupsPool.GetObject();

                break;
            case Interactables.KEY:

                item = keysPool.GetObject();

                break;
        }

        item.transform.position = spawnData.position;
        item.transform.rotation = spawnData.rotation;

        item.transform.rotation = Quaternion.LookRotation(item.transform.forward, spawnData.normal);

        item.Setup(this);

        item.gameObject.SetActive(true);
    }
}
