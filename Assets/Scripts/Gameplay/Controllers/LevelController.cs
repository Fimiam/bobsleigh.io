﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : GameplayController
{
    private Level level;

    public Level Level => level;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        StartCoroutine(LoadingLevel());
    }

    private IEnumerator LoadingLevel()
    {
        SceneManager.LoadSceneAsync(Game.Instance.GetCurrentLevel().Scene, LoadSceneMode.Additive);

        while(level == null)
        {
            level = FindObjectOfType<Level>();

            yield return null;
        }

        SceneManager.SetActiveScene(level.gameObject.scene);

        gameplay.LevelLoaded();
    }
}
