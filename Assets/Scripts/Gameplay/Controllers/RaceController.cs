﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceController : GameplayController
{
    private InputController inputController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        inputController = gameplay.GetController<InputController>();
    }


    private void Update()
    {
        if (isRacing) return;

        if(inputController.HaveTap && !inputController.IsPointerOverUI)
        {
            gameplay.StartRaceReques();
        }
    }
}
