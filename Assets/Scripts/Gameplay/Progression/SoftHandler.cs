﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftHandler
{
    private SoftValueView softView;
    private int collectedSoft;

    public int CollectedSoft => collectedSoft;
    
    private int currentChallangeSoft, nextSoftChallange;

    private bool isChallange;

    public SoftHandler(SoftValueView softView)
    {
        this.softView = softView;
        
        currentChallangeSoft = Game.Instance.Challanges.GetChallangeValue(ChallangeType.SoftCollector);
        
        CheckForNextChallange();
    }
    
    public void AddSoft(Soft soft)
    {
        collectedSoft += soft.Value;
        
        softView.AddingSoft(soft.Value, soft.transform.position);
        
        currentChallangeSoft += soft.Value;

        if (isChallange && currentChallangeSoft >= nextSoftChallange)
        {
            Game.Instance.Challanges.ApplyChallange(ChallangeType.SoftCollector, currentChallangeSoft);
            
            CheckForNextChallange();
        }
    }

    private void CheckForNextChallange()
    {
        var nextChallange = Game.Instance.Configurations.Challanges.GetNextChallange(ChallangeType.SoftCollector, currentChallangeSoft);

        isChallange = nextChallange != null;

        if (isChallange)
        {
            nextSoftChallange = nextChallange.CompleteValue;
            
            Debug.Log($"current soft challange - {nextSoftChallange}");
        }
    }

    public void LevelEnd() //TODO need call on level end
    {
        Game.Instance.Challanges.ApplyChallange(ChallangeType.SoftCollector, currentChallangeSoft); 
    }
}
