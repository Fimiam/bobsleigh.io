﻿using System;
using PathCreation;
using UnityEditor;
using UnityEngine;

public class Road : MonoBehaviour
{
    [SerializeField]
    private Road nextRoad;

    [SerializeField]
    private RoadSurface roadSurface;

    [SerializeField]
    private PathCreator pathCreator;

    private VertexPath path;

    public VertexPath Path => path;

    [SerializeField]
    private RoadSavedData roadData;

    [SerializeField] private RoadProgressData progressData;

    public RoadProgressData ProgressData => progressData;
    
    public float Length { get; private set; }

    public void RacerLanded(Racer racer)
    {
        Vector3 racerPosition = racer.transform.position;

        float distanceOnRoad = path.GetClosestDistanceAlongPath(racerPosition);

        Vector3 point = path.GetPointAtDistance(distanceOnRoad);

        Vector3 cross = Vector3.Cross(path.GetDirectionAtDistance(distanceOnRoad), path.GetNormalAtDistance(distanceOnRoad));

        float distaceToPoint = Vector3.Distance(racerPosition, point);

        float dot = Vector3.Dot(-cross.normalized, (racerPosition - point).normalized);

        float sideShift = dot * distaceToPoint;

        racer.LandingOnRoad(this, distanceOnRoad, sideShift);
    }

    public float Size { get; private set; }
    public float Width { get; private set; }

    private float halfWidth;

    public ProfileQuadraticData horCurvData { get; private set; }
    public ProfileQuadraticData horNormData { get; private set; }

    public Road NextRoad => nextRoad;
    public bool HasNextRoad => nextRoad != null;

    public void Setup()
    {
        roadSurface.Setup(this);

        path = pathCreator.path;

        Length = path.length;

        Size = roadData.size;

        Width = roadData.width;

        halfWidth = Width * .5f;

        horCurvData = new ProfileQuadraticData() 
        {
            left = roadData.horizontalOffsetStart,
            right = roadData.horizontalOffsetEnd,
            control = roadData.horizontalOffsetControl
        };

        horNormData = new ProfileQuadraticData()
        {
            left = roadData.horizontalNormalStart,
            right = roadData.horizontalNormalEnd,
            control = roadData.horizontalNormalControl
        };
    }

    public void RoadInfluance(RacerState racer)
    {
        racer.SideShift = Mathf.Clamp(racer.SideShift, -halfWidth, halfWidth);
    }

    public RoadPoint GetPoint(float distance, float sideShift = 0)
    {
        sideShift = GetTimeSideShift(sideShift);

        RoadPoint rp = new RoadPoint();

        rp.position = path.GetPointAtDistance(distance);
        rp.rotation = path.GetRotationAtDistance(distance);
        rp.normalDirection = rp.rotation * MyMath.QuadtraticBezierPoint(sideShift, horNormData.left, horNormData.right, horNormData.control);

        rp.roadSurfaceOffset = rp.rotation * MyMath.QuadtraticBezierPoint(sideShift, horCurvData.left, horCurvData.right, horCurvData.control) * Size;

        return rp;
    }

    public float GetDistanceAtTime(float t)
    {
        return t * Length;
    }

    public float GetTimeSideShift(float sideShift)
    {
        return (halfWidth + sideShift) / Width;
    }

#if UNITY_EDITOR
    [ContextMenu("save road")]
    public void SaveRoad()
    {
        PathCreator pathCreator = GetComponent<PathCreator>();

        RoadGenerator roadGenerator = GetComponent<RoadGenerator>();

        if (roadGenerator == null) Debug.LogError("there is no RoadGenerator component attached to this object");

        if (pathCreator == null) Debug.LogError("there is no PathCreator component attached to this object");

        roadData = new RoadSavedData(pathCreator, roadGenerator);

        Undo.RecordObject(this, "road data changed");
    }
#endif

    public Vector3 GetRoadPosition(float distTime, float shiftTime, float height)
    {
        Vector3 position = Vector3.zero;

        if(pathCreator != null)
        {
            position = pathCreator.path.GetPointAtTime(distTime);

            position += pathCreator.path.GetRotation(distTime) * MyMath.QuadtraticBezierPoint(shiftTime, roadData.horizontalOffsetStart, roadData.horizontalOffsetEnd, roadData.horizontalOffsetControl) * roadData.size;

            position += pathCreator.path.GetRotation(distTime) * MyMath.QuadtraticBezierPoint(shiftTime, roadData.horizontalNormalStart, roadData.horizontalNormalEnd, roadData.horizontalNormalControl) * height;
        }

        return position;
    }

    public RoadPoint GetRoadPoint(float time, float sideShift, float height)
    {
        RoadPoint rp = new RoadPoint();

        rp.position = pathCreator.path.GetPointAtTime(time);

        rp.rotation = pathCreator.path.GetRotation(time);

        rp.normalDirection = rp.rotation * MyMath.QuadtraticBezierPoint(sideShift, horNormData.left, horNormData.right, horNormData.control) * height;

        rp.roadSurfaceOffset = rp.rotation * MyMath.QuadtraticBezierPoint(sideShift, horCurvData.left, horCurvData.right, horCurvData.control) * Size;

        return rp;
    }

    public float GetLength()
    {
        float dist = 0;

        if(pathCreator != null)
        {
            dist = pathCreator.path.length;
        }

        return dist;
    }
}

public struct RoadPoint
{
    public Vector3 position;
    public Vector3 roadSurfaceOffset;
    public Quaternion rotation;
    public Vector3 normalDirection;
}

public struct ProfileQuadraticData //Quadratic bezier
{
    public Vector3 left;
    public Vector3 right;
    public Vector3 control;
}

[Serializable]
public struct RoadProgressData
{
    public float startProgress;
    public float endProgress;
}
