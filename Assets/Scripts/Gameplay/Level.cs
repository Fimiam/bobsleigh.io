﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class Level : MonoBehaviour
{
    [SerializeField]
    private Road startRoad;

    [SerializeField]
    [Range(0, 1)]
    private float startProgress;


    [SerializeField]
    private Track track;

    [SerializeField]
    private Camera camera;

    [SerializeField]
    private List<MapInteractable> interactables;

    public Camera Camera => camera;

    public Track Track => track;

    public Road StartRoad => startRoad;

    public float StartProgress => startProgress;

#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        if(startRoad != null && startRoad.GetComponent<PathCreator>() != null)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(startRoad.GetComponent<PathCreator>().path.GetPointAtTime(startProgress), Vector3.one * 2);
        }
    }

#endif

    public void SpawnInteractables(InteractablesController interactablesController)
    {
        StartCoroutine(InteractablesInitialization(interactablesController));
    }

    private IEnumerator InteractablesInitialization(InteractablesController interactablesController)
    {
        for(int i = 0; i < interactables.Count; i ++)
        {
            interactables[i].Spawn(interactablesController);

            yield return null;
        }
    }
}
