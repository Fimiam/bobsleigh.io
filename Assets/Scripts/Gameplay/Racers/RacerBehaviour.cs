﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class RacerBehaviour : MonoBehaviour
{
    private List<SpeedModifier> speedStates = new List<SpeedModifier>();
    private List<SpeedModifier> sideSpeedStates = new List<SpeedModifier>();
    private List<SpeedModifier> gravityStates = new List<SpeedModifier>();

    protected Racer racer;

    public RacerBehaviour(Racer racer)
    {
        this.racer = racer;
    }

    public void AddForwardSpeedModifier(SpeedModifier state) => speedStates.Add(state);
    public void AddSideSpeedModifier(SpeedModifier state) => sideSpeedStates.Add(state);
    public void AddGravityModifier(SpeedModifier state) => gravityStates.Add(state);

    public void ForwardSpeedModifierDone(SpeedModifier state) => speedStates.Remove(state);

    public abstract void OutroadJump();
    public abstract void OutroadFallout();

    public void SideSpeedModifierDone(SpeedModifier state) => sideSpeedStates.Remove(state);
    public void GravityModifierDone(SpeedModifier state) => gravityStates.Remove(state);


    public void RemoveGravityModifiers() => gravityStates.Clear();

    public virtual void UpdateBehaviour() 
    {
        racer.State.SideSpeed = GetSideSpeed();
        racer.State.Speed = GetSpeed();
    }

    protected float GetSpeed()
    {
        float result = 0;

        foreach(var s in speedStates)
        {
            result += s.Speed;
        }

        return result;
    }

    protected float GetSideSpeed()
    {
        float result = 0;

        foreach (var s in sideSpeedStates)
        {
            result += s.Speed;
        }

        return result;
    }

    protected float GetGravity()
    {
        float result = 0;

        foreach (var s in gravityStates)
        {
            result += s.Speed;
        }

        return result;
    }

    public void AddSpeedModifier(SpeedModifier speedModifier)
    {
        switch (speedModifier.SpeedType)
        {
            case SpeedType.FORWARD:

                AddForwardSpeedModifier(speedModifier);

                break;
            case SpeedType.SIDE:

                AddSideSpeedModifier(speedModifier);

                break;
            case SpeedType.GRAVITY:

                AddGravityModifier(speedModifier);

                break;
        }
    }

    public void RemoveSpeedModifier(SpeedModifier speedModifier)
    {
        switch (speedModifier.SpeedType)
        {
            case SpeedType.FORWARD:

                ForwardSpeedModifierDone(speedModifier);

                break;
            case SpeedType.SIDE:

                SideSpeedModifierDone(speedModifier);

                break;
            case SpeedType.GRAVITY:

                GravityModifierDone(speedModifier);

                break;
        }
    }
}
