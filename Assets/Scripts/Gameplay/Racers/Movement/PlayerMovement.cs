﻿public abstract class PlayerMovement : MovementBehaviour
{
    protected new PlayerRacer racer;

    public PlayerMovement(PlayerRacer racer) : base(racer)
    {
        this.racer = racer;
    }
}