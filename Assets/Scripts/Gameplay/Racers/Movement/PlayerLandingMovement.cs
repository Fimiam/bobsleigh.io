﻿using UnityEngine;
using UnityEditor;

public class PlayerLandingMovement : PlayerMovement
{
    private float sqrDistToLand = .01f;

    private float landingSpeedMultiplier = 1.05f, landingSpeedAcceleration = 1;

    public PlayerLandingMovement(PlayerRacer racer) : base(racer)
    {
    }

    public override void Update()
    {
        landingSpeedMultiplier += landingSpeedAcceleration * Time.deltaTime;
        
        racer.State.Distance += racer.State.Speed * Time.deltaTime;
        
        if(Mathf.Abs(racer.State.SideShift) > racer.State.Road.Width * .5f)
        {
            racer.OutroadJump();

            return;
        }
        
        if (racer.State.Distance > racer.State.Road.Length)
        {
            if (racer.State.Road.HasNextRoad)
            {
                racer.State.Distance = (racer.State.Distance - racer.State.Road.Length);
                racer.State.Road = racer.State.Road.NextRoad;
            }
            else
            {
                racer.OutroadFallout();

                return;
            }
        }
        


        roadPoint = racer.State.Road.GetPoint(racer.State.Distance, racer.State.SideShift);

        Vector3 targetPos = roadPoint.position + roadPoint.roadSurfaceOffset;

        racer.transform.position = Vector3.MoveTowards(racer.transform.position, targetPos, racer.State.Speed * landingSpeedMultiplier * Time.deltaTime);

        Quaternion prevRot = racer.transform.rotation;

        racer.transform.rotation = roadPoint.rotation;

        Quaternion nextRotation = Quaternion.LookRotation(racer.transform.forward, roadPoint.normalDirection);

        Quaternion rot = Quaternion.RotateTowards(prevRot, nextRotation, 160 * Time.deltaTime);

        racer.transform.rotation = rot;
        
        if ((racer.transform.position - targetPos).sqrMagnitude < sqrDistToLand)
        {
            racer.Landed();
        }
    }
}