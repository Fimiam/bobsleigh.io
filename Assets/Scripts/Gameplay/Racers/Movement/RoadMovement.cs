﻿using UnityEngine;

public class RoadMovement : MovementBehaviour
{
    public RoadMovement(Racer racer) : base(racer)
    {
    }

    public override void Update()
    {
        racer.State.Distance += racer.State.Speed * Time.deltaTime;

        if (racer.State.Distance > racer.State.Road.Length)
        {
            if(racer.State.Road.HasNextRoad)
            {
                racer.State.Distance = (racer.State.Distance - racer.State.Road.Length);
                racer.State.Road = racer.State.Road.NextRoad;
            }
            else
            {
                racer.OutroadFallout();

                return;
            }
        }

        if(Mathf.Abs(racer.State.SideShift) > racer.State.Road.Width * .5f)
        {
            racer.OutroadJump();

            return;
        }

        roadPoint = racer.State.Road.GetPoint(racer.State.Distance, racer.State.SideShift);

        racer.transform.position = roadPoint.position + roadPoint.roadSurfaceOffset;
        racer.transform.rotation = roadPoint.rotation;
        racer.transform.rotation = Quaternion.LookRotation(racer.transform.forward, roadPoint.normalDirection);
    }
}