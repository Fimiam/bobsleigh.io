﻿public abstract class MovementBehaviour
{
    protected RoadPoint roadPoint;

    public RoadPoint RoadPoint => roadPoint;

    protected Racer racer;

    public MovementBehaviour(Racer racer)
    {
        this.racer = racer;
    }

    public abstract void Update();
}