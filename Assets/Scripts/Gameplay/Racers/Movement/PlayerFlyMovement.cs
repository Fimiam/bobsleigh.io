﻿using UnityEngine;
using UnityEditor;

public class PlayerFlyMovement : PlayerMovement
{
    private float flyingStabSpeed = 60;

    public PlayerFlyMovement(PlayerRacer racer) : base(racer)
    { 
        
    }

    public override void Update()
    {
        Vector3 forwardDirection = racer.transform.forward;

        forwardDirection.y = 0;
        
        forwardDirection.Normalize();

        racer.transform.position += (forwardDirection * racer
            .State.Speed + Vector3.up * racer.State.Gravity) * Time.deltaTime;
        
        racer.transform.Rotate(Vector3.up * racer.State.AngularShift);

        Quaternion targetRotation = Quaternion.LookRotation(racer.transform.forward, Vector3.up);

        racer.transform.rotation = Quaternion.RotateTowards(racer.transform.rotation, targetRotation, flyingStabSpeed * Time.deltaTime);
    }
}