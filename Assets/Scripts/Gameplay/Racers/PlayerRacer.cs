﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class PlayerRacer : Racer
{
    private new PlayerRacerState state;

    public new PlayerRacerState State => state;

    private PlayerController playerController;

    public void Setup(RacersController racersController, InputController inputController, PlayerController playerController)
    {
        this.playerController = playerController;

        isPlayer = true;

        base.state = state = new PlayerRacerState();

        inputModule = new PlayerInputModule(state, inputController);

        behaviour = new PlayerRacerBehaviour(this);


        base.Setup(racersController);
    }

    public override void OutroadJump()
    {
        base.OutroadJump();

        movement = new PlayerFlyMovement(this);

        playerController.PlayerOutroad();

        Debug.Log("jump");
    }

    public override void OutroadFallout()
    {
        base.OutroadFallout();

        movement = new PlayerFlyMovement(this);

        playerController.PlayerOutroad();
    }

    public override void LandingOnRoad(Road road, float distance, float sideShift)
    {
        behaviour.RemoveGravityModifiers();
        
        base.LandingOnRoad(road, distance, sideShift);

        movement = new PlayerLandingMovement(this);
    }

    public override void Landed()
    {
        base.Landed();

        movement = new RoadMovement(this);
    }
}
