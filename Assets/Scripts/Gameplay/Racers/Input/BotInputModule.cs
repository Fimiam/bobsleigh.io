﻿using UnityEngine;
using UnityEditor;

public class BotInputModule : InputModule
{
    public BotInputModule(RacerState state) : base(state)
    {

    }

    public override void ProcessInput()
    {
        state.SideShift += Mathf.SmoothStep(.1f, .9f, Mathf.Sin(Time.realtimeSinceStartup * .5f + .5f)) * state.SideSpeed;
    }
}