﻿using UnityEngine;
using UnityEditor;

public abstract class InputModule
{
    protected RacerState state;

    public InputModule(RacerState state) => this.state = state;

    public abstract void ProcessInput();
}