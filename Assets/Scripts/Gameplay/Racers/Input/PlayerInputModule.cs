﻿using UnityEngine;
using UnityEditor;

public class PlayerInputModule : InputModule
{
    private new PlayerRacerState state;

    private InputController inputController;

    public PlayerInputModule(PlayerRacerState state, InputController inputController) : base(state)
    {
        this.state = state;
        this.inputController = inputController;
    }

    public override void ProcessInput()
    {
        state.SideShift += inputController.InputDelta.x * state.SideSpeed;
        state.AngularShift += inputController.InputDelta.x * 2000f;
        state.AngularShift *= Time.deltaTime * 0.99f;
    }
}