﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRacerBehaviour : RacerBehaviour
{
    private PlayerController playerController;

    private new PlayerRacer racer;

    public PlayerRacerBehaviour(PlayerRacer racer) : base(racer)
    {
        this.racer = racer;
    }

    public override void OutroadJump()
    {     
        AddGravityModifier(new TwoTransitionSpeed(racer, -18.5f, SpeedType.GRAVITY, 18, .13f));

        racer.Sleigh.LeftRoad();
    }

    public override void OutroadFallout()
    {
        AddGravityModifier(new TwoTransitionSpeed(racer, -8.5f, SpeedType.GRAVITY, 18, .13f));

        racer.Sleigh.LeftRoad();
    }

    public override void UpdateBehaviour()
    {
        racer.State.Gravity = GetGravity();
        
        base.UpdateBehaviour();
    }
}
