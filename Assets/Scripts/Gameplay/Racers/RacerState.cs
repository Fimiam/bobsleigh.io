﻿public class RacerState
{
    public float Speed;
    public float Distance;
    public float SideShift;
    public float SideSpeed;
    public Road Road;
    public bool IsPlayer;
}

public class PlayerRacerState : RacerState
{
    public float AngularShift;
    public float Gravity;
    public bool IsLanding;
}
