﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour
{
    [SerializeField]
    private List<Road> roads;

    public void Setup() => roads.ForEach(r => r.Setup());
}
