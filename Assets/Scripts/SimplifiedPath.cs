﻿using PathCreation.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

[System.Serializable]
public class RoadSavedData
{
    public Vector3 horizontalOffsetStart, horizontalOffsetEnd, horizontalOffsetControl; // Quadratic bezier curve
    public Vector3 horizontalNormalStart, horizontalNormalEnd, horizontalNormalControl; // Quadratic bezier curve

    public float length;

    public float size, width;

    public RoadSavedData(PathCreator pathCreator, RoadGenerator roadGenerator)
    {
        size = roadGenerator.Size;

        length = pathCreator.path.length;

        horizontalOffsetStart = roadGenerator.Pattern.start * size;
        horizontalOffsetEnd = roadGenerator.Pattern.end * size;
        horizontalOffsetControl = roadGenerator.Pattern.control * size;
        horizontalNormalStart = roadGenerator.Pattern.left * size;
        horizontalNormalEnd = roadGenerator.Pattern.right * size;
        horizontalNormalControl = roadGenerator.Pattern.center * size;

        width = (Vector3.Distance(horizontalOffsetStart, horizontalOffsetControl) +
            Vector3.Distance(horizontalOffsetControl, horizontalOffsetEnd));

        //float progress = 0;

        //PathPoint p;

        //points = (int)(pathCreator.path.length / (pathCreator.bezierPath.NumPoints * (RoadGenerator.QUALITY_MODIFIER / 3)) * roadGenerator.Quality);

        //for (int i = 0; i < points + 1; i++)
        //{
        //    progress = i / (float)points;

        //    if (progress >= 1.0f) progress = .9995f;

        //    p = new PathPoint();

        //    p.Progress = progress;

        //    p.Distance = progress * length;

        //    p.Position = pathCreator.path.GetPointAtTime(progress);

        //    p.Normal = pathCreator.path.GetNormal(progress);

        //    p.Direction = pathCreator.path.GetDirection(progress);

        //    path.Add(p);
        //}
    }

    //public Vector3 GetPointAtTime(float t)
    //{
    //    Debug.Log(t);

    //    int p1 = (int)(t * points);
    //    int p2 = (int)(t * points) + 1;

    //    if (p2 >= points) p2 = p1;

    //    float delta = (t - path[p1].Progress) / (path[p2].Progress - path[p1].Progress);

    //    if (float.IsNaN(delta)) delta = 1.0f;

    //    return Vector3.Lerp(path[p1].Position, path[p2].Position, delta);
    //}

    //public float GetClosestTime(Vector3 position)
    //{
    //    Vector3 closestPoint = Vector3.zero;
    //    Vector3 closestOnLine = Vector3.zero;

    //    float minSqrMgn = float.MaxValue;

    //    float sqrMgn;

    //    int closestA = 0;
    //    int closestB = 0;

    //    int nextI;

    //    for (int i = 0; i < points; i++)
    //    {
    //        nextI = i + 1 > points ? points : i + 1;

    //        closestOnLine = MathUtility.ClosestPointOnLineSegment(position, path[i].Position, path[nextI].Position);

    //        sqrMgn = (position - closestOnLine).sqrMagnitude;

    //        if (sqrMgn < minSqrMgn)
    //        {
    //            minSqrMgn = sqrMgn;

    //            closestPoint = closestOnLine;

    //            closestA = i;
    //            closestB = nextI;
    //        }
    //    }

    //    float t = (closestPoint - path[closestA].Position).sqrMagnitude / (path[closestA].Position - path[closestB].Position).sqrMagnitude;

    //    //Debug.Log($"{closestA} - {closestB}");

    //    return Mathf.Lerp(path[closestA].Progress, path[closestB].Progress, t);
    //}
}

[System.Serializable]
public class PathPoint
{
    public Vector3 Position;
    public Vector3 Direction;
    public Vector3 Normal;
    public float Progress;
    public float Distance;
}
