﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "road pattern", menuName = "Game Configurations/road Pattern")]
public class RoadPattern : ScriptableObject
{
    public List<Vertex> points;

    public List<int> lineIndices;

    [Header("Quadratic surface curvature")]
    public Vector3 start;
    public Vector3 end;
    public Vector3 control;

    [Header("Quadratic surface normal")]
    public Vector3 left;
    public Vector3 center;
    public Vector3 right;

    [Header("Surface collider data")]
    public SurfaceColliderData colliderSurfaceData;
}

[System.Serializable]
public struct Vertex
{
    public Vector2 position;
    public Vector2 normal;
    public float uCoord;
}

[System.Serializable]
public struct SurfaceColliderData
{
    public List<Vector2> positions;
    public List<int> lines;
}
