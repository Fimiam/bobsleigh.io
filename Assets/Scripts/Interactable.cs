﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : PooledObject
{
    public Interactables InteractableType;
}

public struct SpawnItemData
{
    public Vector3 position;
    public Vector3 normal;
    public Quaternion rotation;
}
