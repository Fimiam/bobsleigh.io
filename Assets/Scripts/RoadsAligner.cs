﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

#if UNITY_EDITOR

[RequireComponent(typeof(PathCreator))]
[RequireComponent(typeof(RoadGenerator))]
public class RoadsAligner : MonoBehaviour
{
    [SerializeField]
    private PathCreator alignTo;

    [ContextMenu("Align")]
    public void Align()
    {
        var path = GetComponent<PathCreator>().bezierPath;

        Vector3 alignPos = alignTo.bezierPath[0];
        Vector3 alignControl = alignTo.bezierPath[1];

        path.MovePoint(path.NumPoints - 1, alignPos);
        path.MovePoint(path.NumPoints - 2, alignPos + ((alignControl - alignPos) * -1.0f));

        path.NotifyPathModified();

        GetComponent<RoadGenerator>().ForceGeneration();
    }
}

#endif