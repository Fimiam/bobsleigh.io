﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CollectedSoftEffect : PooledObject
{
    private Camera camera;
    private Canvas canvas;

    [SerializeField] private new RectTransform transform;
    
    public void Setup(Camera camera, Canvas canvas)
    {
        this.camera = camera;
        this.canvas = canvas;
    }
    
    public void ShowEffect(int value, Vector3 collecPos, SoftValueView view)
    {
        Vector2 startVpPos = camera.WorldToViewportPoint(collecPos);
        
        RectTransform canvasRect = canvas.GetComponent<RectTransform>();

        Vector2 startScreenPos = new Vector2(
            ((startVpPos.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
            ((startVpPos.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));
        
        transform.anchoredPosition = startScreenPos;
        
        gameObject.SetActive(true);

        Vector2 endVpPos = camera.ScreenToViewportPoint(view.GetShownScreenPos());
        
        Vector3 targetAnchorPos = new Vector2(
            ((endVpPos.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
            ((endVpPos.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));

        var sequence = DOTween.Sequence();

        sequence.onComplete += () =>
        {
            view.AddValue(value);
            gameObject.SetActive(false);
        };

        Vector2 randomShift = Random.insideUnitCircle * 100;

        sequence.Append(transform.DOAnchorPos(transform.anchoredPosition + randomShift, Random.value * .5f));

        sequence.Append(transform.DOAnchorPos(targetAnchorPos, Random.Range(1, 2)));
    }
    
}
