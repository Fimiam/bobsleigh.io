﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMapInteractable : MapInteractableItem
{
    [SerializeField]
    private List<MapInteractableItem> interactables;

    public override void Spawn(InteractablesController interactablesController)
    {
        var item = interactables[Random.Range(0, interactables.Count)];

        if (item != null)
        {
            item.Spawn(interactablesController);
        }
        else
        {
            Debug.LogError($"the is no items in random array of type - {InteractableType}");
        }
    }
}
