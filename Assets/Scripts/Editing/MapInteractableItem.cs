﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInteractableItem : MapInteractable
{
    [SerializeField]
    protected Road road;

    [SerializeField]
    [Range(0, 1)]
    protected float roadDistance, roadShift = .5f;

    [SerializeField]
    protected float height = 1;

    public override void Spawn(InteractablesController interactablesController)
    {
        RoadPoint roadPoint = road.GetRoadPoint(roadDistance, roadShift, height);

        var spawnData = new SpawnItemData();

        spawnData.position = roadPoint.position + roadPoint.roadSurfaceOffset + roadPoint.normalDirection;
        spawnData.rotation = roadPoint.rotation;
        spawnData.normal = roadPoint.normalDirection;

        interactablesController.InteractableSpawn(InteractableType, spawnData);
    }

#if UNITY_EDITOR

    protected virtual void OnDrawGizmos()
    {
        if (road == null) return;

        Vector3 positon = road.GetRoadPosition(roadDistance, roadShift, height);

        DrawGizmosShape(positon);

        UnityEditor.Handles.Label(positon, $"{InteractableType}");
    }

    protected void DrawGizmosShape(Vector3 position)
    {
        switch (InteractableType)
        {
            case Interactables.SOFT:

                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(position, .5f);

                break;
            case Interactables.HARD:

                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(position, .5f);

                break;
            case Interactables.SPEEDUP:

                Gizmos.color = Color.magenta;
                Gizmos.DrawWireCube(position, Vector3.one * .5f);

                break;
            case Interactables.KEY:

                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(position, .5f);

                break;
        }

    }

#endif

}