﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MapInteractable : Interactable
{
    public abstract void Spawn(InteractablesController interactablesController);
}
