﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInteractableItems : MapInteractableItem
{

    [SerializeField]
    private float distanceBetween = 2f;

    [SerializeField]
    [Range(0, 1)]
    protected float endRoadDistance = 1.0f, endRoadShift = .5f;

#if UNITY_EDITOR

    protected override void OnDrawGizmos()
    {
        if (road == null) return;

        distanceBetween = distanceBetween < .1f ? .1f : distanceBetween;

        int count =  Mathf.RoundToInt((endRoadDistance - roadDistance) * road.GetLength() / distanceBetween);


        float time, shift;

        for(int i = 0; i < count; i ++)
        {
            time = Mathf.Lerp(roadDistance, endRoadDistance, i / (float)count);

            shift = Mathf.Lerp(roadShift, endRoadShift, i / (float)count);

            Vector3 positon = road.GetRoadPosition(time, shift, height);

            DrawGizmosShape(positon);

            UnityEditor.Handles.Label(positon, $"{InteractableType}");
        }
    }

#endif

    public override void Spawn(InteractablesController interactablesController)
    {
        StartCoroutine(Spawning(interactablesController));
    }

    private IEnumerator Spawning(InteractablesController interactablesController)
    {
        RoadPoint roadPoint = new RoadPoint();

        var spawnData = new SpawnItemData();


        distanceBetween = distanceBetween < .1f ? .1f : distanceBetween;

        int count = Mathf.RoundToInt((endRoadDistance - roadDistance) * road.GetLength() / distanceBetween);

        float time, shift;


        for (int i = 0; i < count; i++)
        {
            time = Mathf.Lerp(roadDistance, endRoadDistance, i / (float)count);

            shift = Mathf.Lerp(roadShift, endRoadShift, i / (float)count);

            roadPoint = road.GetRoadPoint(time, shift, height);

            spawnData.position = roadPoint.position + roadPoint.roadSurfaceOffset + roadPoint.normalDirection;
            spawnData.rotation = roadPoint.rotation;
            spawnData.normal = roadPoint.normalDirection;

            interactablesController.InteractableSpawn(InteractableType, spawnData);

            yield return null;
        }
    }
}
