﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSurface : MonoBehaviour
{
    private Road road;

    public Road Road => road;

    public void Setup(Road road)
    {
        this.road = road;
    }
}
