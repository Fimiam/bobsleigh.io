﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "config", menuName = "GameConfigs/mainConfig")]
public class GameConfigurations : ScriptableObject
{
    public PlayerConfigs PlayerConfigs;
    public LevelsConfig LevelsConfig;
    public CommonConfigs CommonConfigs;
    public InputConfigs InputConfigs;
    public DriversConfig Drivers;
    public SleighsConfig Sleighs;
    public SpeedupsComboConfig SpeedupsCombo;
    public ChallangeConfigs Challanges;
}


[System.Serializable]
public struct Opponents
{
    [Range(0, 100)]
    public float BlockChance;

    public float StrikesFrequency;
}

[System.Serializable]
public struct CommonConfigs
{
    public float finalPunchPower;
}

[System.Serializable]
public struct LevelsConfig
{
    public List<LevelConfig> Levels;

    public int LevelsCount => Levels.Count;
}


[System.Serializable]
public struct PlayerConfigs
{

}

[System.Serializable]
public struct SpeedupsComboConfig
{
    public float ComboLastDuration;
    
    public List<SpeedupComboData> datas;

    public string GetComboText(int combo)
    {
        var data = datas.Find(d => d.combo == combo);

        if (data == null) return "Cool!";

        return data.text;
    }
    
    [Serializable]
    public class SpeedupComboData
    {
        public int combo;
        public string text;
    }
}

[System.Serializable]
public struct SleighsConfig
{
    public int DefaultSleigh;

    public List<Sleigh> Sleighs;
}

[System.Serializable]
public struct DriversConfig
{
    public int DefaultDriver;

    public List<Driver> Drivers;
}

[System.Serializable]
public struct ChallangeConfigs
{
    public List<ChallangeConfig> challanges;

    public ChallangeConfig GetNextChallange(ChallangeType type, int currentValue)
    {
        ChallangeConfig challange = null;

        var availableChallanges = challanges.FindAll(c => c.Type == type);

        for (int i = 0; i < availableChallanges.Count; i++)
        {
            if (availableChallanges[i].CompleteValue > currentValue)
            {
                challange = availableChallanges[i];
                
                break;;
            }
        }

        return challange;
    }
}


[System.Serializable]
public struct InputConfigs
{
    
}

[System.Serializable]
public struct VibrationConfigs
{
    public VibrationConfigs.Android AndroidVibration;

    [System.Serializable]
    public struct Android
    {
        public int Shoot;
        public int Crystal;
        public int Death;
        public int Buttons;

        public long[] KillerPattern;
        public long[] WinPattern;
    }
}
