﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationsManager
{
    public void ItemInteractionVibro()
    {
        Vibration.VibratePop();
    }

    public void PlayerOutroadVibration()
    {
        Vibration.VibratePop();
    }

    public void WrongChoiceVibro()
    {
        Vibration.VibratePeek();
    }

    public void CoinVibro()
    {
        Vibration.VibratePop();
    }

    public void LevelCompleteVibro()
    {
        Vibration.VibrateNope();
    }
}
