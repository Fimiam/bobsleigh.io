﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistanceManager
{
    private List<SavingData> datas;

    public PersistanceManager()
    {
        datas = new List<SavingData>();

        datas.Add(InitData<ProgressData>("progress"));
        datas.Add(InitData<StateData>("state"));
        datas.Add(InitData<ChallangesData>("challenges"));
    }

    public T GetData<T>() where T : SavingData
    {
        return datas.Find(d => d.GetType() == typeof(T)) as T;
    }

    public SavingData InitData<T>(string _savingKey) where T : SavingData, new()
    {
        T data;

        if (ZPlayerPrefs.HasKey(_savingKey))
        {
            string toLoad = ZPlayerPrefs.GetString(_savingKey);

            data = JsonUtility.FromJson<T>(toLoad);
        }
        else
        {
            data = new T();
        }

        data.SetKey(_savingKey);

        return data;
    }
}

[Serializable]
public class SavingData
{
    protected string savingKey;

    public void SetKey(string _key) => savingKey = _key;

    protected void Save()
    {
        string toSave = JsonUtility.ToJson(this);

        ZPlayerPrefs.SetString(savingKey, toSave);
        ZPlayerPrefs.Save();
    }
}

[Serializable]
public class SoundsData : SavingData
{
    public event Action<bool> OnSoundsChanged;
    public event Action<bool> OnVibrationChanged;

    public bool SoundActive;
    public bool VibrationFull;

    public SoundsData()
    {
        SoundActive = true;
        VibrationFull = true;
    }

    public void SetSound(bool _active)
    {
        SoundActive = _active;
        OnSoundsChanged?.Invoke(_active);
        Save();
    }

    public void SetVibration(bool _full)
    {
        VibrationFull = _full;
        OnVibrationChanged?.Invoke(_full);
        Save();
    }
}

[Serializable]
public class StateData : SavingData
{
    public bool IsQuickStart;

    public StateData()
    {
        IsQuickStart = false;
    }

    public void SetQuickStart(bool value)
    {
        IsQuickStart = value;

        Save();
    }
}


[Serializable]
public class ProgressData : SavingData
{
    public Action<int> OnSoftChanged;
    public Action<int> OnHardChanged;
    public Action<int> OnStarsChanged;

    public int SoftCurrency;
    public int HardCurrency;
    public int Stars;

    public int ProgressionLevel;
    public int CurrentLevel;
    public int LocationLevel;

    public string FirstLaunchDate;

    public void IncreaseProgressionLevel()
    {
        ProgressionLevel++;

        Save();
    }

    public void SetCurrentLevel(int level)
    {
        CurrentLevel = level;

        Save();
    }

    public void AddSoft(int amount)
    {
        SoftCurrency += amount;

        OnSoftChanged?.Invoke(SoftCurrency);

        Save();
    }

    public void UseSoft(int amount)
    {
        SoftCurrency -= amount;

        SoftCurrency = SoftCurrency < 0 ? 0 : SoftCurrency;

        OnSoftChanged?.Invoke(SoftCurrency);

        Save();
    }

    public void AddStars(int amount)
    {
        Stars += amount;

        Save();
    }

    public ProgressData()
    {
        FirstLaunchDate = DateTime.UtcNow.ToString("yyyy.MM.dd HH-mm-ss");
    }
}

[Serializable]
public class ChallangesData : SavingData
{
    
    public Action<ChallangeType, int> OnChallangeApplied;
    
    public List<ChallangeSaveData> SaveDatas;
    
    public ChallangesData()
    {
        SaveDatas = new List<ChallangeSaveData>();
    }

    public int GetChallangeValue(ChallangeType type)
    {
        var data = SaveDatas.Find(d => d.Type == type);
        
        if (data == null)
        {
            data = new ChallangeSaveData(type);
            
            SaveDatas.Add(data);
        }

        Save();
        
        return data.Value;
    }
    
    public void ApplyChallange(ChallangeType type, int value)
    {
        var data = SaveDatas.Find(d => d.Type == type);

        if (data == null)
        {
            data = new ChallangeSaveData(type);
            
            SaveDatas.Add(data);
        }

        data.Value = value;
        
        OnChallangeApplied?.Invoke(data.Type, data.Value);

        Save();
    }
    
    [Serializable]
    public class ChallangeSaveData
    {
        public ChallangeType Type;
        public int Value;

        public ChallangeSaveData(ChallangeType type)
        {
            Type = type;
        }
    }
}