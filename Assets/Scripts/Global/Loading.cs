﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    private const float FADING_DUR = .45f;
    private const float MIN_LOADING_DUR_RELOAD = .5f;
    private const float MIN_LOADING_DUR_LOADING = 1.2f;

   // [SerializeField]
  //  private int loadingSceneIndex = 1;

    //[SerializeField]
   // private int gameSceneIndex = 2;


    [SerializeField]
    private CanvasGroup canvasGroup;

    private int sceneToLoad;

    private void Awake()
    {
        sceneToLoad = ScenesManager.Instance.SceneToLoad;
    }

    void Start()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(ScenesManager.LOADING_SCENE_INDEX));

        StartCoroutine(LoadingScene());       
    }

    private IEnumerator LoadingScene()
    {
        for (float i = 0; i < FADING_DUR; i += Time.unscaledDeltaTime)
        {
            canvasGroup.alpha = i / FADING_DUR;

            yield return null;
        }


        canvasGroup.alpha = 1.0f;

        for(int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene loadedScene = SceneManager.GetSceneAt(i);

            if(loadedScene.buildIndex != ScenesManager.LOADING_SCENE_INDEX)
            {
                SceneManager.UnloadSceneAsync(loadedScene);
            }
        }

        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        AsyncOperation ao = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);

        ao.allowSceneActivation = false;

        float loadingDur = MIN_LOADING_DUR_LOADING;

        while(ao.progress < .9f)
        {
            loadingDur -= Time.unscaledDeltaTime;

            yield return new WaitForEndOfFrame();
        }

        while(loadingDur > 0)
        {
            loadingDur -= Time.unscaledDeltaTime;

            yield return null;
        }

        ao.allowSceneActivation = true;

        while (!ao.isDone)
        {
            yield return null;
        }

        //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneToLoad));

        for(float i = 0; i < FADING_DUR; i+= Time.unscaledDeltaTime)
        {
            canvasGroup.alpha = 1.0f - i / FADING_DUR;

            yield return null;
        }

        canvasGroup.alpha = 0.0f;

        SceneManager.UnloadSceneAsync(ScenesManager.LOADING_SCENE_INDEX);
    }

    //private IEnumerator ReloadingScene()
    //{
    //    float fadingDur = FADING_DUR;

    //    for (float i = 0; i < fadingDur; i += Time.unscaledDeltaTime)
    //    {
    //        canvasGroup.alpha = i / fadingDur;

    //        yield return new WaitForEndOfFrame();
    //    }

    //    canvasGroup.alpha = 1.0f;

    //    AsyncOperation ao = SceneManager.UnloadSceneAsync(gameSceneIndex);

    //    while(ao.progress < .9f)
    //    {
    //        yield return new WaitForEndOfFrame();
    //    }

    //    ao = SceneManager.LoadSceneAsync(gameSceneIndex);

    //    ao.allowSceneActivation = false;

    //    float loadingDur = MIN_LOADING_DUR_RELOAD;

    //    while (!ao.isDone)
    //    {
    //        loadingDur -= Time.unscaledDeltaTime;

    //        yield return new WaitForEndOfFrame();
    //    }

    //    while (loadingDur > 0)
    //    {
    //        loadingDur -= Time.unscaledDeltaTime;

    //        yield return new WaitForEndOfFrame();
    //    }

    //    ao.allowSceneActivation = true;

    //    while (!ao.isDone)
    //    {
    //        yield return new WaitForEndOfFrame();
    //    }

    //    SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(gameSceneIndex));

    //    fadingDur = FADING_DUR;

    //    for (float i = 0; i < fadingDur; i += Time.unscaledDeltaTime)
    //    {
    //        canvasGroup.alpha = 1.0f - i / fadingDur;

    //        yield return new WaitForEndOfFrame();
    //    }

    //    canvasGroup.alpha = 0.0f;

    //    SceneManager.UnloadSceneAsync(loadingSceneIndex);
    //}
}
