﻿using System.Collections.Generic;
using UnityEngine;

public class Pool<T> : MonoBehaviour where T : PooledObject
{
    [SerializeField]
    private List<T> pool;

    [SerializeField]
    private T prefab;

    public List<T> GetPool() => pool;
    
    public T GetObject()
    {
        T obj = pool.Find(o => !o.IsActive);

        if(obj == null)
        {
            T newObj = Instantiate(prefab);

            newObj.transform.parent = transform;

            pool.Add(newObj);

            obj = newObj;
        }

        return obj as T;
    }
}
