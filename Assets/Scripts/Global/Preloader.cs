﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour
{
    [SerializeField]
    private int toLoad = 1;

    private void Start()
    {
        SceneManager.LoadScene(toLoad);
    }
}
