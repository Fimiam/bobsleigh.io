﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyMath
{
    public static Vector3 QuadtraticBezierPoint(float t, Vector3 a, Vector3 b, Vector3 control)
    {
        return (1.0f - t) * ((1.0f - t) * a + t * control) + t * ((1.0f - t) * control + t * b);
    }
}
