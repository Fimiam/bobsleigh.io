﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : Singleton<ScenesManager>
{
    public const int LOADING_SCENE_INDEX = 1;
    public const int GAMEPLAY_SCENE_INDEX = 2;

    private int sceneToLoad;

    public int SceneToLoad => sceneToLoad;

    public void LoadScene(int index)
    {
        sceneToLoad = index;

        SceneManager.LoadSceneAsync(LOADING_SCENE_INDEX);
    }
}
