﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Game : Singleton<Game>
{
    public static bool IsTabletScreen => (float)Screen.height / (float)Screen.width < 1.5f;

    [SerializeField]
    private GameConfigurations configs;

    public static float COUNTRIES_COUNT { get; internal set; }

    public GameConfigurations Configurations => configs;
    public PersistanceManager Persistance { get; private set; }

    public ProgressData ProgressData { get; private set; }
    public ChallangesData Challanges { get; private set; }
    
    public VibrationsManager Vibrations { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        Persistance = new PersistanceManager();

        ProgressData = Persistance.GetData<ProgressData>();
        
        Vibrations = new VibrationsManager();

        Challanges = Persistance.GetData<ChallangesData>();

        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        //analytics initialization
    }

    public LevelConfig GetCurrentLevel()
    {
        int currLevel = Persistance.GetData<ProgressData>().CurrentLevel;

        if (currLevel >= configs.LevelsConfig.LevelsCount)
        {
            currLevel = 0;

            Persistance.GetData<ProgressData>().SetCurrentLevel(currLevel);
        }

        return configs.LevelsConfig.Levels[currLevel];
    }

    public Sleigh GetSleigh()
    {
        return configs.Sleighs.Sleighs[0];
    }

    public Driver GetDriver()
    {
        return configs.Drivers.Drivers[0];
    }

    public void LevelComplete()
    {
        int nextLevel = ProgressData.CurrentLevel + 1;

        nextLevel = nextLevel >= 0/*configs.LevelsConfig.LevelsCount*/ ? 0 : nextLevel;

        ProgressData.SetCurrentLevel(nextLevel);

        ProgressData.IncreaseProgressionLevel();
    }

#if UNITY_EDITOR

    [MenuItem("Cheating/Add Soft")]
    public static void AddSoft()
    {
        Instance.ProgressData.AddSoft(5000);
    }

#endif
}
