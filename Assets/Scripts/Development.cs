﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Development : MonoBehaviour
{
    [SerializeField] private RoadPattern pattern;
    [SerializeField][Range(0, 1)] private float horizontalTestTime = .5f;


    private void OnDrawGizmos()
    {

        Gizmos.color = Color.green;

        Vector3 pos = MyMath.QuadtraticBezierPoint(horizontalTestTime, pattern.start, pattern.end, pattern.control);

        Gizmos.DrawWireSphere(pos, .5f);

        float step = .02f;

        for (float i = 0; i < .96f; i += step)
        {
            Vector3 p1 = MyMath.QuadtraticBezierPoint(i, pattern.start, pattern.end, pattern.control);
            Vector3 p2 = MyMath.QuadtraticBezierPoint(i + step, pattern.start, pattern.end, pattern.control);

            Gizmos.DrawLine(p1, p2);
        }

        Gizmos.color = Color.white;

        for (int i = 0; i < pattern.points.Count; i++)
        {
            Gizmos.DrawSphere(pattern.points[i].position, .1f);
        }
    }

}
