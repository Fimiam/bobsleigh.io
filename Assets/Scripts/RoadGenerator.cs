﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using UnityEditor;
using System;

[RequireComponent(typeof(PathCreator))]
[ExecuteAlways]
public class RoadGenerator : MonoBehaviour
{
    public const string ROAD_OBJECT_NAME = "road_mesh";
    public const string ROAD_SURFACE_NAME = "road_surface";

    public const int QUALITY_MODIFIER = 10;

    private List<Vector3> vertecies = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();
    private List<int> triangles = new List<int>();

    private Mesh roadMesh, surfaceColliderMesh;

    [SerializeField]
    private RoadPattern pattern;

    [SerializeField]
    private PathCreator pathCreator;

    [SerializeField]
    private Material roadMaterial;

    [SerializeField]
    private float size = 1.0f;

    [SerializeField]
    [Range(0.3f, 20)]
    private float quality = 30;

    public float Size => size;

    public float Quality => quality;

    GameObject roadObject;

    public RoadPattern Pattern => pattern;



    private Transform road, meshCol;

#if UNITY_EDITOR

    [Header("Testing")]

    [SerializeField]
    private bool testUnit = true;
    [SerializeField]
    private bool testSurfaceCurvature = false;

    [SerializeField]
    [Range(0, .999f)]
    private float testTime;

    [SerializeField]
    [Range(0, .999f)]
    private float horizontalTestTime = .5f;

    [SerializeField]
    [Range(.1f, 4)]
    private float testSphereSize = 1.0f;


    private void Start()
    {
        GenerateMesh();
    }

    private void OnDrawGizmos()
    {
        if(testSurfaceCurvature)
        {
            Gizmos.color = Color.green;

            Vector3 pos = QuadtraticBezierPoint(horizontalTestTime, pattern.start, pattern.end, pattern.control);

            Gizmos.DrawWireSphere(pos, .5f);

            float step = .02f;

            for(float i = 0; i < .96f; i+= step)
            {
                Vector3 p1 = QuadtraticBezierPoint(i, pattern.start, pattern.end, pattern.control);
                Vector3 p2 = QuadtraticBezierPoint(i + step, pattern.start, pattern.end, pattern.control);

                Gizmos.DrawLine(p1, p2);
            }

            Gizmos.color = Color.white;

            for(int i = 0; i < pattern.points.Count; i++)
            {
                Gizmos.DrawSphere(pattern.points[i].position, .1f);
            }
        }

        if (testUnit)
        {
            Vector3 a = pathCreator.path.GetPointAtTime(testTime);
            Vector3 dir = pathCreator.path.GetDirection(testTime);
            Quaternion rot = pathCreator.path.GetRotation(testTime);
            a += rot * (QuadtraticBezierPoint(horizontalTestTime, pattern.start, pattern.end, pattern.control) * size);


            Vector3 normal = QuadtraticBezierPoint(horizontalTestTime, pattern.left, pattern.right, pattern.center);

            Gizmos.color = Color.cyan;
            Handles.color = Color.cyan;
            Handles.DrawArrow(1, a, rot, 15);
            Handles.color = Color.green;
            Handles.DrawArrow(1, a, Quaternion.LookRotation(rot * normal), 15);
            Handles.color = Color.white;
            Gizmos.DrawSphere(a, testSphereSize);
        }
        
        if(!Application.isPlaying)
            GenerateMesh();
    }

#endif

    [ContextMenu("generate")]
    public void ForceGeneration() => GenerateMesh();

    private void GenerateMesh()
    {
        if(roadMesh == null)
        {
            SetupMesh();
        }

        vertecies.Clear();
        triangles.Clear();
        normals.Clear();
        roadMesh.Clear();

        Vector3 pos;
        Quaternion rot;

        float t = 0;

        int segments = (int)(pathCreator.path.length / (pathCreator.bezierPath.NumPoints * QUALITY_MODIFIER) * quality);

        for(int segment = 0; segment < segments + 1; segment++)
        {
            t = segment / (float)segments;

            t = t >= 1.0f ? 0.999999f : t;

            pos = pathCreator.path.GetPointAtTime(t);
            rot = pathCreator.path.GetRotation(t);

            for(int v = 0; v < pattern.points.Count; v++)
            {
                var vert = pos + rot * pattern.points[v].position * size;

                vertecies.Add(vert);
                normals.Add(rot * pattern.points[v].normal);
            }
        }

        int rootIndex;
        int nextRootIndex;
        int lineIndexA;
        int lineIndexB;
        int currentA, nextA, currentB, nextB;

        for(int segment = 0; segment < segments; segment++)
        {
            rootIndex = segment * pattern.points.Count;
            nextRootIndex = (segment + 1) * pattern.points.Count;

            for(int line = 0; line < pattern.lineIndices.Count; line += 2)
            {
                lineIndexA = pattern.lineIndices[line];
                lineIndexB = pattern.lineIndices[line + 1];

                currentA = rootIndex + lineIndexA;
                currentB = rootIndex + lineIndexB;
                nextA = nextRootIndex + lineIndexA;
                nextB = nextRootIndex + lineIndexB;

                triangles.Add(currentA);
                triangles.Add(nextA);
                triangles.Add(nextB);
                triangles.Add(nextB);
                triangles.Add(currentB);
                triangles.Add(currentA);
            }
        }

        roadMesh.SetVertices(vertecies);
        roadMesh.SetTriangles(triangles, 0);
        roadMesh.SetNormals(normals);

        CreateColliderSurface();
    }

    private void CreateColliderSurface()
    {
        if(surfaceColliderMesh == null)
        {
            meshCol = transform.Find(ROAD_SURFACE_NAME);

            if(meshCol == null)
            {
                surfaceColliderMesh = new Mesh();

                surfaceColliderMesh.name = "road_surface";

                meshCol = new GameObject(ROAD_SURFACE_NAME).transform;

                meshCol.parent = transform;

                meshCol.gameObject.AddComponent<MeshFilter>().mesh = surfaceColliderMesh;

                meshCol.gameObject.AddComponent<MeshRenderer>().enabled = false;

                meshCol.gameObject.AddComponent<MeshCollider>();
            }
            else
            {
                surfaceColliderMesh = meshCol.GetComponent<MeshFilter>().sharedMesh;
            }
        }

        List<Vector3> vertecies = new List<Vector3>();
        List<int> triangles = new List<int>();

        Vector3 pos;
        Quaternion rot;

        float t = 0;

        int segments = (int)((pathCreator.path.length / (pathCreator.bezierPath.NumPoints * QUALITY_MODIFIER) * quality) * .45f);

        for (int segment = 0; segment < segments + 1; segment++)
        {
            t = segment / (float)segments;

            t = t >= 1.0f ? 0.999999f : t;

            pos = pathCreator.path.GetPointAtTime(t);
            rot = pathCreator.path.GetRotation(t);

            for (int v = 0; v < pattern.colliderSurfaceData.positions.Count; v++)
            {
                var vert = pos + rot * pattern.colliderSurfaceData.positions[v] * size;

                vertecies.Add(vert);
            }
        }

        int rootIndex;
        int nextRootIndex;
        int lineIndexA;
        int lineIndexB;
        int currentA, nextA, currentB, nextB;

        for (int segment = 0; segment < segments; segment++)
        {
            rootIndex = segment * pattern.colliderSurfaceData.positions.Count;
            nextRootIndex = (segment + 1) * pattern.colliderSurfaceData.positions.Count; ;

            for (int line = 0; line < pattern.colliderSurfaceData.lines.Count; line += 2)
            {
                lineIndexA = pattern.colliderSurfaceData.lines[line];
                lineIndexB = pattern.colliderSurfaceData.lines[line + 1];

                currentA = rootIndex + lineIndexA;
                currentB = rootIndex + lineIndexB;
                nextA = nextRootIndex + lineIndexA;
                nextB = nextRootIndex + lineIndexB;

                triangles.Add(currentA);
                triangles.Add(nextA);
                triangles.Add(nextB);
                triangles.Add(nextB);
                triangles.Add(currentB);
                triangles.Add(currentA);
            }
        }

        surfaceColliderMesh.SetVertices(vertecies);
    
        //Debug.Log(triangles.Count);
        
        for (int i = 0; i < triangles.Count; i++)
        {
            if (triangles[i] > vertecies.Count - 1)
            {
                Debug.Log($"{i} - {triangles[i]}");
            }
        }
        
        surfaceColliderMesh.SetTriangles(triangles, 0);
    }

    private void SetupMesh()
    {
        road = transform.Find(ROAD_OBJECT_NAME);

        if (road == null)
        {
            roadMesh = new Mesh();

            roadMesh.name = "road";

            road = new GameObject(ROAD_OBJECT_NAME).transform;

            road.parent = transform;

            road.gameObject.AddComponent<MeshFilter>().mesh = roadMesh;   

            road.gameObject.AddComponent<MeshRenderer>().material = roadMaterial;
        }
        else
        {
            roadMesh = road.gameObject.GetComponent<MeshFilter>().sharedMesh;
        }
    }

    private Vector3 QuadtraticBezierPoint(float t, Vector3 a, Vector3 b, Vector3 control)
    {
        return (1.0f - t) * ((1.0f - t) * a + t * control) + t * ((1.0f - t) * control + t * b);
    }
}