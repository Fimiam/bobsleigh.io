﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testVectors : MonoBehaviour
{
    [SerializeField]
    private float angle;

    
    void Update()
    {
        float radAngle = angle * Mathf.Deg2Rad;

        transform.position = new Vector3(Mathf.Cos(radAngle), 0, Mathf.Sin(radAngle));
    }
}
